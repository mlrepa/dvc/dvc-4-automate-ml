#Tutorial: Automate ML pipelines with DVC
*From Jupyter Notebooks to Reproducible and Automated experiments with DVC in just 4 steps*

# Demo Project Structure
------------------------
```
    .
    ├── data
    │   ├── processed               <- processed data
    │   └── raw                     <- original unmodified/raw data
    ├── models                      <- folder for ML models
    ├── notebooks                   <- Jupyter Notebokos (ingored by Git)
    ├── reports                     <- folder for experiment reports
    ├── src                         <- source code for modules & pipelines
    └── README.md
```

## Preparation

### 1. Fork / Clone this repository

### 2. Create and activate virtual environment

Create virtual environment named `.venv` (you may use other name)
```bash
python3 -m venv .venv
echo "export PYTHONPATH=$PWD" >> .venv/bin/activate
source .venv/bin/activate
```
Install python libraries

```bash
pip install -r requirements.txt
```

Add Virtual Environment to Jupyter Notebook

```bash
python -m ipykernel install --user --name=dvc-4-automate-ml
``` 

## 3. Run Jupyter Lab

```bash
jupyter lab
```

## Tutorial 
    
#### Step 0: All in Junyter Notebooks 
- run all in Jupyter Notebooks

#### Step 1: Create a Single configuration file 

- a separate section for each logical stage 
- one base section with common configs (random_state)
- human readable format (.yaml)
- 
#### Step 2: Move code to .py modules
- i.e. main funcitons and classes 

#### Step 3: Create pipeline

Add a pipeline stages code to `src/pipelines`

    featurize.py - create new features
    split_train_test.py - split source dataset into train/test
    train.p - train classifier 
    evaluate.py - evaluate model and create metrics file

    
#### Step 4: Automate experiment pipeline with DVC
  
- add pipelines dependencies under DVC control
- add models/data/configs under DVC control


## References for code examples used

1. [DVC tutorial](https://dvc.org/doc/tutorial)
2. [Plot a Confusion Matrix](https://www.kaggle.com/grfiv4/plot-a-confusion-matrix) 